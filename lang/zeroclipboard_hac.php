<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zeroclipboard?lang_cible=hac
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'link_text_copied' => 'کۆپی',
	'link_text_copy' => 'کۆپیکەردەی',
	'link_title_copied' => 'لینکو سەردېڕی کۆپیکریا',
	'link_title_copy' => 'کۆپیکەردەو لینکو سەردېڕی'
);
