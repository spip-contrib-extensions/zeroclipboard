<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-zeroclipboard?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zeroclipboard_description' => 'Добавляет ссылку  "Копировать" перед каждым элементом страницы с классом <code>.copypaste</code> и при нажатии на нее добавляет содержимое в буфер обмена компьютера (Ctrl-C)',
	'zeroclipboard_nom' => 'Zeroclipboard',
	'zeroclipboard_slogan' => 'Позволяет создать ссылку "Скопировать" на сайте'
);
