<?php
/**
 * Plugin Zeroclipboard
 * 
 * @package SPIP\Zeroclipboard\Options
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * On définit le chemin du répertoire _DIR_LIB_ZEROCLIPBOARD
 */
define('_DIR_LIB_ZEROCLIPBOARD',_DIR_LIB.'zeroclipboard-2.2.0/');
?>